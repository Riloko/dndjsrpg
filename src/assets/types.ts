export type TLocation = {
  $timerElement: HTMLElement;
  $canvasElement: HTMLElement;
  type: string;
}
