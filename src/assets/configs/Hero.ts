import * as HeroImage from '../Hero.png';

export const HeroConfig = {
  asset: HeroImage,
  char: {
    LVL: 1,
    STR: 5,
    DEX: 4,
    INT: 2,
    EXP: 0
  },
  type: 'HERO'
};
