import * as EnemyImage from '../Enemy.png';

export const EnemyConfig = {
  asset: EnemyImage,
  char: {
    LVL: 1,
    STR: 5,
    DEX: 4,
    INT: 2,
    EXP: 0
  },
  type: 'ENEMY'
};
