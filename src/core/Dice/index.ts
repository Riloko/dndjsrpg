import styles from './dice.module.scss';

export default class Dice {
  edges: 4 | 6 | 8 | 10 | 12 | 20 | 24 | 30 | 60;
  tries: number;
  $container: HTMLElement;


  constructor(edges: 4 | 6 | 8 | 10 | 12 | 20 | 24 | 30 | 60, tries: number) {
    this.edges = edges;
    this.tries = tries;
    this.$container = null;

    this.init();
  }

  private init(): void {
    const $root = document.createElement('div');
    $root.classList.add(styles.element);

    this.$container = $root;
  }

  private rollDice(): number {
    return Math.floor( Math.random() * this.edges ) + 1;
  }

  public makeMove(): void {
    let total = 0;
    for (let i = 0; i < this.tries; i++) {
      const value = this.rollDice();
      const $dice = document.createElement('div');

      $dice.classList.add(styles.element__dice);
      $dice.textContent = String(value);
      if (this.edges === 20) {
        (value === 1) && ($dice.style.color = 'red');
        (value === 20) && ($dice.style.color = 'green');
      }
      total += value;

      this.$container.insertAdjacentElement('beforeend', $dice);
    }
    console.log(total);
  }

}
