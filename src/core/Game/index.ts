import GameLayout from "../../components/layout";
import Personage from '../Personage';
import Battle from "../Battle";
import Dice from "../Dice";

import { HeroConfig } from "../../assets/configs/Hero";
import { EnemyConfig } from "../../assets/configs/Enemy";



export default class Game {
  container: HTMLElement;
  layout: GameLayout;
  hero: Personage;
  enemy: Personage;
  battle: Battle;
  state: string;

  constructor(container: HTMLElement) {
    this.container = container;
    this.layout = null;
    this.hero = null;
    this.enemy = null;
    this.state = null;
    this.battle = null;

    this._init();
  }

  private _init(): void {
    const dice = new Dice(20, 1);
    console.log(dice)

    this.layout = new GameLayout();

    this.container.insertAdjacentElement('beforeend', this.layout.hood);
    this.hero = new Personage({ ...HeroConfig, $container: this.layout.personage });
    this.enemy = new Personage({ ...EnemyConfig, $container: this.layout.enemy });
    this.state = 'idle';
    this.layout.canvas.insertAdjacentElement('beforeend', dice.$container);

    dice.makeMove();

  }

  private startBattle(enemy: Personage): void {
    this.battle = new Battle(this.hero, enemy, { $timerElement: this.layout.timer, $canvasElement: this.layout.canvas, type: 'SAMPLE_LOCATION' } );
  }


};
