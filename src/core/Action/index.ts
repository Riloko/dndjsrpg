



export default class Action {
  title: string;
  description: string;
  type: string;
  target: string;

  constructor() {

  }
}
