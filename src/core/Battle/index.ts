import Personage from "../Personage";
import { TLocation } from "../../assets/types";

type TBattleLogs = {
  round: number;
  log: string;
}

export default class Battle {
  hero: Personage;
  enemy: Personage;
  location: TLocation;
  round: number;
  turn: Personage;
  timer: NodeJS.Timer;
  state: string;
  logs: TBattleLogs[];

  constructor(hero: Personage, enemy: Personage, location: TLocation) {
    this.hero = hero;
    this.enemy = enemy;
    this.location = location;
    this.round = 0;
    this.turn = null;
    this.timer = null;
    this.state = null;
    this.logs = [];

    this.init();
  }

  private init() {
   this.turn = this.checkInitiative();
   this.round = 1;
   this.state = 'STARTED';
   this.startTimer();

  }

  private checkInitiative(): Personage {
    return this.hero.characteristics.Char.DEX >= this.enemy.characteristics.Char.DEX ? this.hero : this.enemy;
  }

  private startTimer() {
    const self = this;
    const deadline = new Date(new Date().getFullYear(), new Date().getMonth(),  new Date().getDate(), new Date().getHours(), new Date().getMinutes() + 1, new Date().getSeconds());

    function countdownTimer()  {
      const now = new Date();

      const diff = <any>deadline - <any>now;
      if (diff <= 0) { clearInterval(self.timer); self.nextTurn(); };

      const minutes = diff > 0 ? Math.floor(diff / 1000 / 60) % 60 : 0;
      const seconds = diff > 0 ? Math.floor(diff / 1000) % 60 : 0;

      self.location.$timerElement.textContent = `${("0" + minutes).slice(-2)} : ${("0" + seconds).slice(-2)}`;

    }
    countdownTimer();
    this.timer = setInterval(countdownTimer, 1000);


  }

  private nextTurn(): void {
    this.timer && clearInterval(this.timer);

    this.turn = this.turn.type === 'HERO' ? this.enemy : this.hero;
    this.round++;
    this.startTimer();
  }

  private logger(logData: TBattleLogs): void {
    this.logs.push({
      round: logData.round,
      log: logData.log
    })
  }

}
