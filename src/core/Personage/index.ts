import styles from './personage.module.scss';

import Characteristics from "./Characteristics";
import { TConfig } from "./types";



export default class Personage {
  $root: HTMLElement;
  $personage: HTMLElement;
  asset: string;
  characteristics: Characteristics;
  type: string;

  constructor(config: TConfig) {
    this.$root = config.$container;
    this.$personage = null;
    this.characteristics = new Characteristics(config.char);
    this.asset = config.asset;
    this.type = config.type;

    this.createPersonage();
  }


  private createPersonage(): void {
    if (!this.asset) return console.error('Asset undefined');

    const $root = document.createElement('div');
    const $rootInner = document.createElement('div');
    const $rootInnerPersonage = document.createElement('div');
    const $rootInnerHp = document.createElement('div');
    const $rootInnerIcons = document.createElement('div');
    const $PersonageImage = document.createElement('img');

    $PersonageImage.setAttribute('src', this.asset);
    $PersonageImage.setAttribute('alt', 'personage');

    $root.classList.add(styles.element);
    $rootInner.classList.add(styles.element__inner);
    $rootInnerPersonage.classList.add(styles.element__inner_personageContainer);
    $rootInnerHp.classList.add(styles.element__inner_hpContainer);
    $rootInnerIcons.classList.add(styles.element__inner_iconsContainer);

    $rootInnerHp.insertAdjacentElement('beforeend', this.characteristics.HPBar.$element);
    $rootInnerPersonage.insertAdjacentElement('beforeend', $PersonageImage);
    $rootInner.insertAdjacentElement('beforeend', $rootInnerPersonage);
    $rootInner.insertAdjacentElement('beforeend', $rootInnerHp);
    $rootInner.insertAdjacentElement('beforeend', $rootInnerIcons);
    $root.insertAdjacentElement('beforeend', $rootInner);

    this.$personage = $root;
    this.$root.insertAdjacentElement('beforeend', $root);
  }

  public attack(enemy: Personage): void {
    enemy.characteristics.loseHp(3);
    console.log(enemy);
  }
};
