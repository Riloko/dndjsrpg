export type THPBar = {
  $element: HTMLElement;
  $HPBar: HTMLElement;
  $HPBarData: HTMLElement;
}

// HP = 20STR
// MP = 10INT

export type TChar = {
  LVL: number;
  STR: number;
  DEX: number;
  INT: number;
  EXP?: number;
};

export type TResources = {
  HP: number;
  MP: number;
}

// DODGE_CHANCE = ( DEX / (20 * LVL) ) * 100

export type TChances = {
  DODGE_CHANCE: number;
}

export type TConfig = {
  asset: string;
  char: TChar;
  $container: HTMLElement;
  type: string;
}
