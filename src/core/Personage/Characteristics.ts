import styles from './hpbar.module.scss';
import { TChances, TChar, THPBar, TResources } from "./types";



export default class Characteristics {
  Char: TChar;
  Chances: TChances;
  Resources: TResources;
  CurrResources: TResources;
  HPBar: THPBar;



  constructor(char: TChar) {
    const self = this;

    this.HPBar = { $element: null, $HPBar: null, $HPBarData: null };
    this.Char = char;
    this.Chances = { DODGE_CHANCE: ( self.Char.DEX / (20 * self.Char.LVL) ) * 100 };
    this.Resources = { HP: 20 * self.Char.STR, MP: 10 * self.Char.INT };
    this.CurrResources = { HP: 20 * self.Char.STR, MP: 10 * self.Char.INT };


    this.createHPBar();
  }

  private createHPBar(): void {
    const $root = document.createElement('div');
    const $rootInner = document.createElement('div');
    const $rootInnerProgress = document.createElement('div');
    const $rootStatusInner = document.createElement('div');
    const $rootStatusInnerData = document.createElement('div');

    $root.classList.add(styles.element);
    $rootInner.classList.add(styles.element__inner);
    $rootInnerProgress.classList.add(styles.element__inner_progress);
    $rootStatusInner.classList.add(styles.element__statusInner);
    $rootStatusInnerData.classList.add(styles.element__statusInner_data);

    $rootStatusInner.insertAdjacentElement('beforeend', $rootStatusInnerData);
    $rootInner.insertAdjacentElement('beforeend', $rootInnerProgress);
    $root.insertAdjacentElement('beforeend', $rootInner);
    $root.insertAdjacentElement('beforeend', $rootStatusInner);

    this.HPBar.$element = $root;
    this.HPBar.$HPBar = $rootInnerProgress;
    this.HPBar.$HPBarData = $rootStatusInnerData;
    this.setCurrentHP(this.Resources.HP);
  }

  private setCurrentHP( currHP: number ): void {
    const perc = (currHP / this.Resources.HP ) * 100;
    let color = null;

    (perc > 0 && perc < 25) && (color = 'red');
    (perc >= 25 && perc < 80) && (color = 'yellow');
    (perc >= 80 && perc <= 100) && (color = 'green');

    this.HPBar.$HPBar.style.width = `${perc}%`;
    this.HPBar.$HPBar.style.background = color;
    this.HPBar.$HPBarData.textContent = `${this.CurrResources.HP} / ${this.Resources.HP}`;
  }

  public loseHp(dmg: number): void {
    this.CurrResources.HP -= dmg;
    this.setCurrentHP(this.CurrResources.HP);
  }


}
