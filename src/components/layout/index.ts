import styles from './assets/layout.module.scss';


export default class GameLayout {
  hood: HTMLElement;
  personage: HTMLElement;
  location: HTMLElement;
  enemy: HTMLElement;
  timer: HTMLElement;
  canvas: HTMLElement;
  actions: HTMLElement;

  constructor() {
    this.hood = null;
    this.personage = null;
    this.location = null;
    this.enemy = null;
    this.timer = null;
    this.canvas = null;
    this.actions = null;

    this.createHood()
  }


  private createHood() {
    const $root = document.createElement('div');
    const $rootTop = document.createElement('div');
    const $rootBottom = document.createElement('div');
    const $rootTopHero = document.createElement('div');
    const $rootTopEnemy = document.createElement('div');

    $root.classList.add(styles.element);
    $rootTop.classList.add(styles.element__topSide);
    $rootBottom.classList.add(styles.element__bottomSide);
    $rootTopHero.classList.add(styles.element__topSide__hero);
    $rootTopEnemy.classList.add(styles.element__topSide__enemy);

    $rootTop.insertAdjacentElement('beforeend', $rootTopHero);
    $rootTop.insertAdjacentElement('beforeend', this.createLocationBlock());
    $rootTop.insertAdjacentElement('beforeend', $rootTopEnemy);
    $root.insertAdjacentElement('beforeend', $rootTop);
    $root.insertAdjacentElement('beforeend', $rootBottom);

    this.hood = $root;
    this.personage = $rootTopHero;
    this.enemy = $rootTopEnemy;
  }

  private createLocationBlock(): HTMLElement {
    const $root = document.createElement('div');
    const $rootInner = document.createElement('div');
    const $rootInnerTop = document.createElement('div');
    const $rootInnerTopTimer = document.createElement('div');
    const $rootInnerMain = document.createElement('div');
    const $rootInnerBottom = document.createElement('div');
    const $rootInnerBottomInner = document.createElement('div');
    const $rootInnerBottomInnerActions = document.createElement('div');

    $root.classList.add(styles.element__topSide__location);
    $rootInner.classList.add(styles.element__topSide__location__inner);
    $rootInnerTop.classList.add(styles.element__topSide__location__inner_top);
    $rootInnerTopTimer.classList.add(styles.element__topSide__location__inner_top_timer);
    $rootInnerMain.classList.add(styles.element__topSide__location__inner_main);
    $rootInnerBottom.classList.add(styles.element__topSide__location__inner_bottom);
    $rootInnerBottomInner.classList.add(styles.element__topSide__location__inner_bottom__inner);
    $rootInnerBottomInnerActions.classList.add(styles.element__topSide__location__inner_bottom__inner_actions);

    $rootInnerBottomInner.insertAdjacentElement('beforeend', $rootInnerBottomInnerActions);
    $rootInnerBottom.insertAdjacentElement('beforeend', $rootInnerBottomInner);
    $rootInnerTop.insertAdjacentElement('beforeend', $rootInnerTopTimer);
    $rootInner.insertAdjacentElement('beforeend', $rootInnerTop);
    $rootInner.insertAdjacentElement('beforeend', $rootInnerMain);
    $rootInner.insertAdjacentElement('beforeend', $rootInnerBottom);
    $root.insertAdjacentElement('beforeend', $rootInner);

    this.location = $root;
    this.timer = $rootInnerTopTimer;
    this.canvas = $rootInnerMain;
    this.actions = $rootInnerBottomInnerActions;

    return $root;
  }
}
