const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const isDevelopment = process.env.NODE_ENV === 'development'

module.exports = {
    mode: 'development',
    entry: './src/index.ts',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './public',
        port: 7700,
        hot: true,
    },
    plugins: [
        new HtmlWebpackPlugin({
          title: 'Development',
          template: "./public/index.html",
          inject: 'body'
        }),
        new MiniCssExtractPlugin({
            filename: isDevelopment ? '[name].css' : '[name].[contenthash].css',
            chunkFilename: isDevelopment ? '[id].css' : '[id].[contenthash].css'
      })
    ],
    output: {
        filename: 'bundle.[contenthash].js',
        path: path.resolve(__dirname, '/public'),
        publicPath: '/',
        assetModuleFilename: 'assets/[contenthash][ext][query]',
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            { test: /\.tsx?$/, loader: "ts-loader" },
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                  loader: "babel-loader",
                  options: {
                    presets: ['@babel/preset-env']
                  }
                }
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
          {
            test: /\.s[ac]ss$/i,
            oneOf: [
              {
                test: /\.module\.s[ac]ss$/,
                use: [
                  MiniCssExtractPlugin.loader,
                  {
                    loader: "css-loader",
                    options: {
                      importLoaders: 1,
                      modules: {
                        mode: "local",
                        localIdentName: "[name]__[local]__[hash:base64:5]"
                      },
                    }
                  },
                  "sass-loader"
                ]
              },
              {
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
              }
            ]
          },
            {
                test: /\.(png|jpg|jpeg|gif)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.svg$/,
                loader: 'svg-inline-loader'
            }
        ]
    },
    optimization: {
        moduleIds: 'deterministic',
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                  test: /[\\/]node_modules[\\/]/,
                  name: 'vendors',
                  chunks: 'all',
                },
            },
        },
    },
};
